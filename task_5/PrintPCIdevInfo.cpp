/*
* Print information about PCI device
*
* 
*/

#include "stdafx.h"
#include <winioctl.h>

extern BOOL verifyDeviceInstanceID(
    _In_ HDEVINFO &hDevInfo,
    _In_ SP_DEVINFO_DATA &DeviceInfoData,
    _In_ TCHAR *devInstanceID);

TCHAR *getDevProperty(HDEVINFO &hDevInfo, SP_DEVINFO_DATA &DeviceInfoData, DWORD DevProperty) {
    DWORD DataT;
    TCHAR *buffer = NULL;
    DWORD buffersize = 0;

    //
    // Call function with null to begin with, 
    // then use the returned buffer size (doubled)
    // to Alloc the buffer. Keep calling until
    // success or an unknown failure.
    //
    //  Double the returned buffersize to correct
    //  for underlying legacy CM functions that 
    //  return an incorrect buffersize value on 
    //  DBCS/MBCS systems.
    // 
    //retrieves a specified Plug and Play device property.
    while (!SetupDiGetDeviceRegistryProperty(
        hDevInfo,
        &DeviceInfoData,
        DevProperty, //The function retrieves a REG_SZ string that contains the description of a device.
        &DataT, // A pointer to a variable that receives the data type of the property that is being retrieved (may be NULL)
        (PBYTE)buffer,  // A pointer to a buffer that receives the property that is being retrieved.
        buffersize,     // current size of @buffer
        &buffersize))   // a required size of @buffer to to hold the data for the requested property.
    {
        if (ERROR_INSUFFICIENT_BUFFER == GetLastError())
        {
            // Change the buffer size.
            if (buffer) {
                delete[] buffer;
            }
            // Double the size to avoid problems on 
            // W2k MBCS systems per KB 888609. 
            buffer = (TCHAR *)new TCHAR[buffersize * 2 * sizeof(TCHAR)];
        }
        else
        {
            // Insert error handling here.
            break;
        }
    }

    return buffer;
}

#define PRINT_DEV_PROPERTY(hDevInfo, DeviceInfoData, Property, fmt)     \
    do {                                                                \
        TCHAR *buf = getDevProperty(hDevInfo, DeviceInfoData, Property);\
        if (buf) { PRINT(fmt, buf); delete[] buf; }                     \
    } while (0)



void printPCIdeviceInfo(TCHAR *devInstanceID) {
    HDEVINFO hDevInfo;
    SP_DEVINFO_DATA DeviceInfoData;
    DWORD i;

    setlocale(LC_ALL, "");

    PRINT("PCI DEVICE (parent)\n");

    // get a handle to a device information set
    // that contains all installed devices that matched the supplied parameters
    hDevInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_STORAGEPORT,
        NULL, // Enumerator
        0,
        DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
    if (INVALID_HANDLE_VALUE == hDevInfo) {
        // Insert error handling here.
        return;
    }

    // Enumerate through all devices in @hDevInfo Set

    // The caller must set DeviceInfoData.cbSize to sizeof(SP_DEVINFO_DATA).
    DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    for (i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &DeviceInfoData); i++) {
        // find device which DeviceInstanceID corresponds to @devInstanceID
        if (!verifyDeviceInstanceID(hDevInfo, DeviceInfoData, devInstanceID))
            continue;

        PRINT_DEV_PROPERTY(hDevInfo, DeviceInfoData, SPDRP_DEVICEDESC, "deviceDescription = %s\n");
        PRINT_DEV_PROPERTY(hDevInfo, DeviceInfoData, SPDRP_BUSNUMBER, "busnumber = %s\n");
        PRINT_DEV_PROPERTY(hDevInfo, DeviceInfoData, SPDRP_LOCATION_INFORMATION, "locationInfo = %s\n");
    }


    if (GetLastError() != NO_ERROR &&
        GetLastError() != ERROR_NO_MORE_ITEMS)
    {
        // Insert error handling here.
        return;
    }

    //  Cleanup
    SetupDiDestroyDeviceInfoList(hDevInfo);
    return;
}