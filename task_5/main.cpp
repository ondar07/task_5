
#include "stdafx.h"

// see in volumes.cpp
extern void defineVolumes(void);

extern std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > disksStructures;
extern std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > cdromsStructures;

BOOL get_DEVINFO_DATA_and_INTERFACE_DEV_DETAIL_DATA_structures(
    _In_ GUID &guid,
    _Out_ std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > &deviceStructures
    )
{
    HDEVINFO hDevInfo = SetupDiGetClassDevs(&guid, 0, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
    if (hDevInfo == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    std::vector<std::pair<SP_DEVINFO_DATA, SP_DEVICE_INTERFACE_DATA> > interfaces;

    // get information about all devices in the device information set
    // by calling SetupDiEnumDeviceInfo() function
    for (DWORD i = 0; true; i++)
    {
        SP_DEVINFO_DATA devInfo;    // structure that represents a device information element+
        devInfo.cbSize = sizeof(SP_DEVINFO_DATA);
        BOOL succ = SetupDiEnumDeviceInfo(hDevInfo, i, &devInfo);
        if (!succ) {
            if (ERROR_NO_MORE_ITEMS == GetLastError())
                break;
            else {
                // TODO: error handling
                return FALSE;
            }
        }
        //devInfo.DevInst

        // Enumerates interfaces for the current device (@devInfo)
        // @interfaceIndex is relative to only the interfaces exposed by that device (@devInfo)
        for (DWORD interfaceIndex = 0; ; interfaceIndex++)
        {
            SP_DEVICE_INTERFACE_DATA ifInfo;    // interface info
            ifInfo.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

            if (FALSE == SetupDiEnumDeviceInterfaces(hDevInfo, &devInfo, &guid, interfaceIndex, &ifInfo))
            {
                if (ERROR_NO_MORE_ITEMS == GetLastError())
                    break;
                else {
                    // TODO: error handling;
                    return FALSE;
                }
            }
            interfaces.push_back(std::make_pair(devInfo, ifInfo));
        }
    }

    //std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > devicePaths;
    for (size_t i = 0; i < interfaces.size(); i++)
    {
        DWORD requiredSize = 0;

        // Note: see Remarks in msdn (2-step process):

        // 1 step.
        SetupDiGetDeviceInterfaceDetail(hDevInfo, &(interfaces.at(i).second), NULL, 0, &requiredSize, NULL);
        if (ERROR_INSUFFICIENT_BUFFER != GetLastError()) {
            // TODO: error handling
            return FALSE;
        }

        // 2 step.
        SP_INTERFACE_DEVICE_DETAIL_DATA *detailInterfData = (SP_INTERFACE_DEVICE_DETAIL_DATA *)(new BYTE[requiredSize * sizeof(BYTE)]);
        if (!detailInterfData) {
            // TODO: error handling;
            return FALSE;
        }

        detailInterfData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
        if (!SetupDiGetDeviceInterfaceDetail(hDevInfo, &(interfaces.at(i).second), detailInterfData, requiredSize, NULL, NULL))
        {
            // TODO: ERROR handling;

            // next element
            continue;
        }

        //PRINT("devicePath = %s\n", detailInterfData->DevicePath);
        //getDeviceNumberIOCTL(detailInterfData->DevicePath);
        deviceStructures.push_back(std::make_pair(interfaces.at(i).first, detailInterfData));
    }

    // cleanup
    SetupDiDestroyDeviceInfoList(hDevInfo);
    return TRUE;
}

static void prepare() {
    GUID disksGuid = GUID_DEVINTERFACE_DISK;
    GUID cdromGuid = GUID_DEVINTERFACE_CDROM;

    if (!get_DEVINFO_DATA_and_INTERFACE_DEV_DETAIL_DATA_structures(disksGuid, disksStructures))
        exit(-1);
    if (!get_DEVINFO_DATA_and_INTERFACE_DEV_DETAIL_DATA_structures(cdromGuid, cdromsStructures))
        exit(-1);
}

int _tmain(int argc, TCHAR *argv[])
{
    prepare();

    PRINT("\n\nVOLUMES\n");
   
    defineVolumes();

    PRINT("\n\n\n");

    getchar();
    return 0;
}
