#include "stdafx.h"
#include <Cfgmgr32.h>

extern BOOL getStorageDeviceNumberStruct(
    _In_  TCHAR *devPath,
    _Out_ STORAGE_DEVICE_NUMBER &StorDevNum);


static BOOL defineCdromNumber(
    _In_ TCHAR *deviceName,
    _Out_ DWORD &cdromNumber)
{
    STORAGE_DEVICE_NUMBER StorDevNum;

    if (!getStorageDeviceNumberStruct(deviceName, StorDevNum)) {
        return FALSE;
    }

    // if this device is CDROM, set @cdromNumber
    if (FILE_DEVICE_CD_ROM == StorDevNum.DeviceType) {
        cdromNumber = StorDevNum.DeviceNumber;
        return TRUE;
    }

#ifdef DEBUG
    PRINT("%s is NOT cdrom!\n", deviceName);
#endif
    return FALSE;
}

//
// For example:
//     @deviceName == "\\Device\\CdRom0"
//     we should return pointer to 'C' letter in @deviceName
//     i.e. pointer to the first letter after "\\Device\\" substring in @deviceName
static TCHAR *getDeviceNamePart(_In_ TCHAR *deviceName)
{
    // 1. check if "\\Device\\" substring exists in @deviceName
#define STR_SEARCH "\\Device\\"
    if (!_tcsstr(deviceName, STR_SEARCH))
        return NULL;
    // 2. return a pointer to the first letter after "\\Device\\" substring in @deviceName
    return (deviceName + _tcslen(STR_SEARCH));
}
#undef STR_SEARCH

//
// For example:
//      @deviceName == "\\Device\\CdRom0"
//      @deviceNamePart will be "CdRom0" (a pointer to the first letter after "\\Device\\" substring in @deviceName)
//      @symlink will be "\\\\?\\CdRom0"
static TCHAR *getSymlink(_In_ TCHAR *deviceName) {
    TCHAR *symlink;
    DWORD symlinkLength;
    TCHAR *deviceNamePart;

    // 0. get a pointer to the first letter after "\\Device\\" substring in @deviceName
    deviceNamePart = getDeviceNamePart(deviceName);
    if (!deviceNamePart) {
        PRINT("[getSymlink]ERROR: couldn't find \"Device\" substring for DeviceName=%s!\n", deviceName);
        return NULL;
    }

#define SYMLINK_BEGINNING "\\\\?\\"
    // 1. allocate memory for @symlink
    symlinkLength = _tcslen(SYMLINK_BEGINNING) + _tcslen(deviceNamePart) + 1;
    symlink = new TCHAR[symlinkLength * sizeof(TCHAR)];
    if (!symlink) {
        PRINT("[getSymlink]ERROR: couldn't allocate memory for symlink!\n");
        return NULL;
    }

    ZeroMemory(symlink, symlinkLength);

    // 2. fill @symlink
    _tcscat_s(symlink, symlinkLength, SYMLINK_BEGINNING);
    _tcscat_s(symlink, symlinkLength, deviceNamePart);

    return symlink;
}

#undef SYMLINK_BEGINNING


//
// example of @deviceName -- "\\Device\\CdRom[�]" (for example, "\\Device\\CdRom0")
// we should define this number [�] and store it in @cdromNumber
BOOL defineCdromVolumeCorrespondance(
    _In_ TCHAR *deviceName,
    _Out_ DWORD &cdromNumber)
{
    BOOL result = TRUE;
    TCHAR *symlink = NULL; // for example, "\\\\?\\CdRom0"

    // 1.
    symlink = getSymlink(deviceName);
    if (!symlink) {
        result = FALSE;
        goto cleanup_and_exit;
    }

    // 2.
    if (!defineCdromNumber(symlink, cdromNumber)) {
        // this is NOT CDROM!
        result = FALSE;
        goto cleanup_and_exit;
    }

cleanup_and_exit:
    if (symlink)
        delete[] symlink;
    return result;
}