#ifndef _GLOBAL_HEADER_H
#define _GLOBAL_HEADER_H

// for printing messages
#define PRINT(msg, ...) _tprintf(msg, __VA_ARGS__)

// for debugging
#define DEBUG


#endif