#include "stdafx.h"
#include <Cfgmgr32.h>

std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > cdromsStructures;


// see implementations of these functions in printDiskInfo.cpp

extern DWORD getDeviceNumberIOCTL(TCHAR *devPath);
extern void printStorageDevInstance(
    _In_ DEVINST devInst);

void printCdromInfo(DWORD CdromNumber) {
    CONFIGRET status;

    for (DWORD i = 0; i < cdromsStructures.size(); i++) {
        SP_DEVINFO_DATA devInfoData = cdromsStructures[i].first;

        // 1. get disk number
        DWORD cdromNumber = getDeviceNumberIOCTL(cdromsStructures[i].second->DevicePath);

        if (CdromNumber != cdromNumber) {
            continue;
        }

        // 2.
        printStorageDevInstance(cdromsStructures[i].first.DevInst);

        // we find cdrom that corresponds to @CdromNumber
        // so we must break
        break;
    }
    return;
}