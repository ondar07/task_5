/*
 * Print information about disk, using its DiskNumber
 *
 * The main function of this file is printDiskInfo()
 */

#include "stdafx.h"
#include <Cfgmgr32.h>

std::vector<std::pair<SP_DEVINFO_DATA, SP_INTERFACE_DEVICE_DETAIL_DATA *> > disksStructures;

BOOL getStorageDeviceNumberStruct(
    _In_  TCHAR *devPath,
    _Out_ STORAGE_DEVICE_NUMBER &StorDevNum)
{
    HANDLE hDevice = INVALID_HANDLE_VALUE;
    BOOL Result;
    DWORD bytesReturned;

    hDevice = CreateFile(devPath,
        0,
        FILE_SHARE_READ |
        FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if (INVALID_HANDLE_VALUE == hDevice) {
#ifdef DEBUG
        PRINT("[getDeviceNumberIOCTL]ERROR: couldn't open a device %s\n", devPath);
#endif
        return FALSE;
    }

    Result = DeviceIoControl(
        hDevice,
        IOCTL_STORAGE_GET_DEVICE_NUMBER,
        NULL,
        0,
        &StorDevNum,
        sizeof(StorDevNum),
        &bytesReturned,
        (LPOVERLAPPED)NULL
        );
    if (0 == Result) {
        // error
        return FALSE;
    }

    CloseHandle(hDevice);
    return TRUE;
}

DWORD getDeviceNumberIOCTL(TCHAR *devPath) {
    STORAGE_DEVICE_NUMBER StorDevNum;

    if (!getStorageDeviceNumberStruct(devPath, StorDevNum))
        return -1;
    return StorDevNum.DeviceNumber;
}

// get DeviceInstanceId (fill @deviceInstanceID) by using device's DEVINST @devInst
BOOL getDeviceInstanceID(
    _In_ DEVINST devInst,
    _Out_ TCHAR *deviceInstanceID,
    _In_  DWORD parentDeviceInstanceID_size)
{
    CONFIGRET status;

    ZeroMemory(deviceInstanceID, parentDeviceInstanceID_size);
    status = CM_Get_Device_ID(devInst, deviceInstanceID, parentDeviceInstanceID_size, 0);
    if (status != CR_SUCCESS) {
        return FALSE;
    }
    return TRUE;
}

//
// fill @parentDeviceInstanceID by by parent device instance ID
//
// @currentDevInst -- device instance of current device (child)
// @parentDevInst  -- for parent device
// @parentDeviceInstanceID -- string is filled by parent device instance ID
// @parentDeviceInstanceID_size -- length of @parentDeviceInstanceID (in TCHARs)
//
// return:
//     TRUE, if success
BOOL getParentInstanceID(
    _In_ DEVINST currentDevInst,
    _Out_ DEVINST &parentDevInst,
    _Out_ TCHAR *parentDeviceInstanceID,
    _In_  DWORD parentDeviceInstanceID_size)
{
    CONFIGRET status = FALSE;

    ZeroMemory(parentDeviceInstanceID, parentDeviceInstanceID_size);
    status = CM_Get_Parent(&parentDevInst, currentDevInst, 0);
    if (status != CR_SUCCESS) {
#ifdef DEBUG
        PRINT("[]ERROR: CM_Get_Parent does NOT return CR_SUCCESS\n");
#endif
        return FALSE;
    }

    if (!getDeviceInstanceID(parentDevInst, parentDeviceInstanceID, parentDeviceInstanceID_size)) {
#ifdef DEBUG
        PRINT("[]ERROR: CM_Get_Device_ID does NOT return CR_SUCCESS\n");
#endif
        return FALSE;
    }

    return TRUE;
}

// this function detects if the device's (@hDevInfo) DeviceInstanceID value is
// equivalent to @devInstanceID
// return TRUE, if these values are equivalent
// else return FALSE
BOOL verifyDeviceInstanceID(
    _In_ HDEVINFO &hDevInfo,
    _In_ SP_DEVINFO_DATA &DeviceInfoData,
    _In_ TCHAR *devInstanceID)
{
    BOOL status;
    TCHAR curInstanceID[MAX_DEVICE_ID_LEN];

    // 1. define DeviceInstanceID (@curInstanceID) for this device (@DeviceInfoData)
    ZeroMemory(curInstanceID, MAX_DEVICE_ID_LEN);
    status = SetupDiGetDeviceInstanceId(
        hDevInfo,
        &DeviceInfoData,
        curInstanceID,
        MAX_DEVICE_ID_LEN,
        NULL);
    if (!status) {
        return FALSE;
    }

    // 2. compare two TCHAR strings
    if (_tcscmp(devInstanceID, curInstanceID) != 0) {
        return FALSE;
    }
    return TRUE;
}

extern void printPCIdeviceInfo(TCHAR *devInstanceID);

void printStorageDevInstance(
    _In_ DEVINST devInst) 
{
    DEVINST devInstParent;
    TCHAR szDeviceInstanceID[MAX_DEVICE_ID_LEN];

    ZeroMemory(szDeviceInstanceID, MAX_DEVICE_ID_LEN);
    if (!getDeviceInstanceID(devInst, szDeviceInstanceID, MAX_DEVICE_ID_LEN)) {
        // TODO: error handling
        return;
    }
    PRINT("DeviceInstanceID=%s\n", szDeviceInstanceID);

    if (!getParentInstanceID(devInst, devInstParent, szDeviceInstanceID, MAX_DEVICE_ID_LEN)) {
        // TODO: error handling
        return;
    }
    PRINT("Parent DeviceInstanceID=%s\n", szDeviceInstanceID);

    // 3. parent is PCI device, so we call this function
    printPCIdeviceInfo(szDeviceInstanceID);
}

void printDiskInfo(DWORD DiskNumber) {
    CONFIGRET status;

    for (DWORD i = 0; i < disksStructures.size(); i++) {
        TCHAR szDeviceInstanceID[MAX_DEVICE_ID_LEN];
        SP_DEVINFO_DATA devInfoData = disksStructures[i].first;

        // 1. get disk number
        DWORD diskNumber = getDeviceNumberIOCTL(disksStructures[i].second->DevicePath);

        if (DiskNumber != diskNumber) {
            continue;
        }

        // 2.
        printStorageDevInstance(devInfoData.DevInst);

        // we find cdrom that corresponds to @CdromNumber
        // so we must break
        break;
    }
}