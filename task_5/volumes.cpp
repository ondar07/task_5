
/*
 * 
 *
 */

#include "stdafx.h"
#include <winioctl.h>


// Retrieves the physical location of a specified volume on one or more disks.
static BOOL getDiskExtentsStructure(
    _In_ LPCTSTR VolumeName,
    _Out_ DWORD &DiskExtentsNr,     // number of elements in @diskExtents array
    _Out_ VOLUME_DISK_EXTENTS **diskExtents)
{
    HANDLE hDevice = INVALID_HANDLE_VALUE;
    BOOL bResult;
    DWORD lpBytesReturned;

    DiskExtentsNr = 1;
    *diskExtents = new VOLUME_DISK_EXTENTS[DiskExtentsNr * sizeof(VOLUME_DISK_EXTENTS)];
    if (NULL == *diskExtents) {
        PRINT("[getDiskExtentsStructure]ERROR: couldn't allocate memory for diskExtents\n");
        goto error_handling;
    }
    ZeroMemory(*diskExtents, DiskExtentsNr * sizeof(VOLUME_DISK_EXTENTS));

    hDevice = CreateFile(VolumeName,
        0,                // ��� ������� � ����������
        FILE_SHARE_READ |
        FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);

    if (INVALID_HANDLE_VALUE == hDevice)
    {
        PRINT("[getDiskExtentsStructure]ERROR: couldn't open a device (volume)\n");
        goto error_handling;
    }

    for (;;)
    {
        bResult = DeviceIoControl(hDevice,
            IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
            NULL, 0, // (input) buffer is NULL (for this operation)
            *diskExtents, sizeof(VOLUME_DISK_EXTENTS) * DiskExtentsNr,
            &lpBytesReturned,
            (LPOVERLAPPED)NULL);

        if (0 != bResult) {
            // success
            break;
        }

        // else
        //      error occurs
        DWORD Error = GetLastError();

        if (ERROR_MORE_DATA == Error ||
            ERROR_INSUFFICIENT_BUFFER == Error)
        {
            if (*diskExtents)
                delete[] * diskExtents;

            DiskExtentsNr++;
            *diskExtents = new VOLUME_DISK_EXTENTS[DiskExtentsNr * sizeof(VOLUME_DISK_EXTENTS)];
            if (NULL == *diskExtents) {
                PRINT("[getDiskExtentsStructure]ERROR: couldn't allocate memory for diskExtents (in cycle)\n");
                goto error_handling;
            }
            ZeroMemory(*diskExtents, DiskExtentsNr * sizeof(VOLUME_DISK_EXTENTS));
            continue;
        }
        else {
            //PRINT("[getDiskExtentsStructure]UNDEFINED ERROR (maybe, this is NOT disk! for example, CDROM)\n");
            goto error_handling;
        }
    }

    CloseHandle(hDevice);

    return (TRUE);

error_handling:

    if (INVALID_HANDLE_VALUE == hDevice)
        CloseHandle(hDevice);

    if (*diskExtents)
        delete[] * diskExtents;

    *diskExtents = NULL;
    DiskExtentsNr = 0;
    return (FALSE);
}


// see comments of defineHddVolumeCorrespondance() function
static void removeTrailingBackslash(_Out_ TCHAR *VolumeGUIDpath) {
    size_t Index = _tcslen(VolumeGUIDpath) - 1;
    VolumeGUIDpath[Index] = '\0';
}


static void restoreTrailingBackslash(_Out_ TCHAR *VolumeGUIDpath) {
    // because in removeTrailingBackslash() function we add '\0' at the end
    // we use only _tcslen(VolumeGUIDpath)
    size_t Index = _tcslen(VolumeGUIDpath);
    VolumeGUIDpath[Index] = '\\';
}

// see in printDiskInfo.cpp
extern void printDiskInfo(DWORD DiskNumber);
// see in printCdRomInfo.cpp
extern void printCdromInfo(DWORD CdromNumber);

static void printVolumeDiskExtentInfo(VOLUME_DISK_EXTENTS &volumeDiskExtent) {
    PRINT("\tNumber of disks in this volume=%ld\n", volumeDiskExtent.NumberOfDiskExtents);
    for (DWORD i = 0; i < volumeDiskExtent.NumberOfDiskExtents; i++) {
        DISK_EXTENT diskExtent = volumeDiskExtent.Extents[i];
        PRINT("\t\tDisk Number=%ld\n", diskExtent.DiskNumber);
        PRINT("DISK INFO\n");
        printDiskInfo(diskExtent.DiskNumber);
    }
}

BOOL defineCdromVolumeCorrespondance(
    _In_ TCHAR *VolumeGUIDpath,
    _Out_ DWORD &cdromNumber);

//
// @lpFileName string argument should be the following form: "\\.\X:" for CreateFile() function.
// Do not use a trailing backslash (\), which indicates the root directory of a drive.
//
// example:
//    @VolumeGUIDpath: "\\\\?\\Volume{9920ed39-03ad-48dd-8c0e-decac9adfb15}\\"
//    we should remove trailing backslash =>
//    => modified VolumeGUIDpath will be equal to "\\\\?\\Volume{9920ed39-03ad-48dd-8c0e-decac9adfb15}"
//    then we must restore it
static BOOL defineHddVolumeCorrespondance(
    _In_ TCHAR *VolumeGUIDpath)
{
    // Represents a physical location on a disk.
    VOLUME_DISK_EXTENTS *diskExtents = NULL;
    DWORD diskExtentsNr = 0;
    BOOL result;

    // 1. remove trailing backslash
    removeTrailingBackslash(VolumeGUIDpath);

    // 2. 
    // WARNING: for CDROM this does NOT WORK (only for disks)
    if (!getDiskExtentsStructure(VolumeGUIDpath,
        diskExtentsNr,
        &diskExtents))
    {
        // this approach does NOT work for CDROM
        result = FALSE;
        goto cleanup_and_exit;
    }

    for (DWORD i = 0; i < diskExtentsNr; i++) {
        printVolumeDiskExtentInfo(diskExtents[i]);
    }

    result = TRUE;

cleanup_and_exit:

    // 3. restore trailing backslash
    restoreTrailingBackslash(VolumeGUIDpath);
    if (diskExtents)
        delete[] diskExtents;
    return result;
}

//
//
void DisplayVolumePaths(TCHAR *VolumeName) {
    DWORD  CharCount = MAX_PATH + 1;
    TCHAR *Names = NULL;
    TCHAR *NameIdx = NULL;
    BOOL   Success = FALSE;

    for (;;)
    {
        //
        //  Allocate a buffer to hold the paths.
        Names = (TCHAR *) new BYTE[CharCount * sizeof(TCHAR)];

        if (!Names) {
            PRINT("[DisplayVolumePaths] ERROR: out of memory for Names\n");
            return;
        }

        //
        //  Obtain all of the paths
        //  for this volume.
        Success = GetVolumePathNamesForVolumeName(
            VolumeName, Names, CharCount, &CharCount
            );

        if (Success)
            break;

        if (GetLastError() != ERROR_MORE_DATA)
            break;

        //
        //  Try again with the
        //  new suggested size.
        delete[] Names;
        Names = NULL;
    }

    if (Success)
    {
        //
        //  Display the various paths.
        for (NameIdx = Names;
            NameIdx[0] != '\0';
            NameIdx += _tcslen(NameIdx) + 1)
        {
            PRINT("  %s", NameIdx);
        }
        PRINT("\n");
    }

    if (Names != NULL) {
        delete[] Names;
        Names = NULL;
    }
    return;
}

void defineVolumes(void)
{
    DWORD  CharCount = 0;
    TCHAR  DeviceName[MAX_PATH];
    DWORD  Error = ERROR_SUCCESS;
    HANDLE FindHandle = INVALID_HANDLE_VALUE;
    BOOL   Found = FALSE;
    size_t Index = 0;
    BOOL   Success = FALSE;
    TCHAR  VolumeName[MAX_PATH];

    //
    //  Enumerate all volumes in the system.
    FindHandle = FindFirstVolume(VolumeName, ARRAYSIZE(VolumeName));

    if (INVALID_HANDLE_VALUE == FindHandle)
    {
        Error = GetLastError();
        PRINT("FindFirstVolume failed with error code %d\n", Error);
        return;
    }

    for (;;)
    {
        //
        //  Skip the \\?\ prefix and remove the trailing backslash.
        // (example: Volume name: \\?\Volume{4c1b02c1-d990-11dc-99ae-806e6f6e6963}\ )
        Index = _tcslen(VolumeName) - 1;

        if (VolumeName[0] != '\\' ||
            VolumeName[1] != '\\' ||
            VolumeName[2] != '?' ||
            VolumeName[3] != '\\' ||
            VolumeName[Index] != '\\')
        {
            Error = ERROR_BAD_PATHNAME;
            PRINT("FindFirstVolumeW/FindNextVolume returned a bad path: %s\n", VolumeName);
            break;
        }

        //  QueryDosDevice does not allow a trailing backslash,
        //  so temporarily remove it.
        VolumeName[Index] = '\0';

        // skip "\\?\" prefix, so use with VolueName[4]
        CharCount = QueryDosDevice(&VolumeName[4], DeviceName, ARRAYSIZE(DeviceName));

        // restore the trailing backslash
        VolumeName[Index] = '\\';

        if (0 == CharCount)
        {
            Error = GetLastError();
            PRINT("QueryDosDevice failed with error code %d\n", Error);
            break;
        }

        PRINT("\nFound a device:\n %s", DeviceName);
        PRINT("\nVolume name: %s", VolumeName);
        PRINT("\nPaths:");
        DisplayVolumePaths(VolumeName);

        // 
        if (!defineHddVolumeCorrespondance(VolumeName)) {
            // approach with DISK_EXTENT does NOT work for CDROM
            // so we try to define correspondance with CDROM 
            // if this is CDROM, print CDROM info

            DWORD cdromNumber;
            if (defineCdromVolumeCorrespondance(DeviceName, cdromNumber))
                printCdromInfo(cdromNumber);
        }

        //  Move on to the next volume.
        Success = FindNextVolume(FindHandle, VolumeName, ARRAYSIZE(VolumeName));

        if (!Success)
        {
            Error = GetLastError();

            if (Error != ERROR_NO_MORE_FILES)
            {
                PRINT("FindNextVolume failed with error code %d\n", Error);
                break;
            }

            //
            //  Finished iterating
            //  through all the volumes.
            Error = ERROR_SUCCESS;
            break;
        }
    }

    FindVolumeClose(FindHandle);
    FindHandle = INVALID_HANDLE_VALUE;

    return;
}
